# EuBic - European Bank Identity Credential 

Use bank-issued verifiable credentials to prove your identity when requesting and presenting your diploma.

yes.com operates a large-scale identity federation of about 1000 financial institutions in Germany. The objective of this project is to extend existing (bank) identity providers with the capability to issue verifiable credentials asserting these bank-verified identities. This will allow graduated students to prove their identity when requesting their diploma (again as verifiable credentials). Subsequently, both credentials can be used in combination to prove the graduation and the identity of the holder, e.g. in the course of a job application. 

The bank identity credential can certainly be used in other scenarios as well, such as onboarding with insurance websites, car sharing, vacation flat rental services, or age verification for gaming. 

The project also aims at underpinning the service with a sustainable business model and suitable security level (e.g. regarding the wallet’s key management). As a result, bank customers can enjoy the freedom of self sovereignty while financial institutions will have an incentive and the security allowing them to contribute their tremendous reach of verified identity data to the European SSI ecosystem. 
